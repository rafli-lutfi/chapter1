const readlineSync = require("readline-sync");

function calc(types, ...inputs) {
  if (types === "+" || types === "tambah" || types === "1") return inputs[0] + inputs[1];
  if (types === "-" || types === "kurang" || types === "2") return inputs[0] - inputs[1];
  if (types === "*" || types === "kali" || types === "3") return inputs[0] * inputs[1];
  if (types === "/" || types === "bagi" || types === "4") return inputs[0] / inputs[1];
  if (types === "square" || types === "pangkat" || types === "5") return inputs[0] ** inputs[1];
  if (types === "root" || types === "akar" || types === "6") return Math.sqrt(inputs[0], inputs[1]);
  if (types === "triangle" || types === "luas segitiga" || types === "segitiga" || types === "7") return (inputs[0] * inputs[1]) / 2;
  if (types === "cube" || types === "volume kubus" || types === "kubus" || types === "8") return inputs[0] ** 3;
  if (types === "cylinder" || types === "tabung" || "volume tabung" || types === "9") return Math.PI * inputs[0] ** 2 * inputs[1];
}

function displayMenu() {
  console.log(`MENU
[1] tambah        || +        
[2] kurang        || -
[3] kali          || *
[4] bagi          || /
[5] pangkat       || square
[6] akar          || root
[7] luas segitiga || triangle   (input1 : alas; input2 : tinggi)
[8] volume kubus  || cube       (input1 : rusuk)
[9] volume tabung || cylinder   (input1 : jari-jari alas; input2: tinggi tabung)
[0] exit          || keluar     
`);

  return readlineSync.prompt({
    limit: [
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "0",
      "+",
      "-",
      "/",
      "*",
      "square",
      "root",
      "tambah",
      "kurang",
      "kali",
      "bagi",
      "akar",
      "pangkat",
      "luas segitiga",
      "triangle",
      "volume kubus",
      "cube",
      "volume tabung",
      "cylinder",
      "exit",
      "keluar",
      "help",
    ],
    limitMessage: "WRONG INPUT [ $<lastInput> ], masukkan sesuai yang ada di Menu",
    caseSensitive: true,
  });
}

function inputValues(command){
  let input1 = readlineSync.questionFloat("masukkan nilai pertama: ");
  let input2;

  if (command != "cube" && command != "volume kubus" && command != "kubus") {
    input2 = readlineSync.questionFloat("masukkan nilai kedua: ");
  }

  return calc(command, input1, input2);
}

let status = false;

while (!status) {
  let command = displayMenu();

  console.log("anda memilih menu " + command + "\n")

  if (command == "help") {
    console.clear();
    continue;
  }
  if (command == "exit" || command == "keluar" || command == 0) {
    status = true;
    continue;
  }

  let result = inputValues(command)

  console.clear();

  console.log("hasilnya adalah: " + result);
  readlineSync.question("press anything ... ");
  
  console.clear();
}
